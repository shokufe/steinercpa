package algorithms;

class RootFinder 
{ 
		int parent;
    	int rank; 
}; 
public class Edge {
	private int source;
	private int destination;
	private double poids;
	Edge(int a,int b , double poids){
		this.source=a;
		this.destination=b;
		this.poids=poids;
	}
	public int getSourceIndex() {
		return source;
	}
	public int getDestIndex() {
		return destination;
	}
	public double edgeCost() {
		return this.poids;
	}	
}
