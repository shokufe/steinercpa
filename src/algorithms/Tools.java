package algorithms;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

public class Tools {
//this methode initialise the graphG(save the weights of each edges) and matrix way(used in Floyd-Warshall) 
	public static void CreateP(ArrayList<Point> points , int[][]way, int edgeThreshold, double[][] graphG) {		
	for (int i=0;i<way.length;i++) {
      for (int j=0;j<way.length;j++) {
        if (i==j) {graphG[i][i]=0; continue;}
        if (points.get(i).distance(points.get(j))<=edgeThreshold)  //if distance elucienne less than threshold, we save it , else we save infinity as weight 
        	graphG[i][j]=points.get(i).distance(points.get(j));
        else 
        	graphG[i][j]=Double.POSITIVE_INFINITY;
        way[i][j]=j;
      }
    }
		
	}
	public static void shorthestPath(double[][] weights, int[][] path,ArrayList<Point> points) {
	//algorithm Floyd-Warshall 
		for(int k=0;k<points.size();k++) {
			for(int i=0;i<points.size();i++) {
				for(int j=0;j<points.size();j++) {
					if ((weights[i][j]> (weights[i][k] + weights[k][j]))){
			            weights[i][j]=weights[i][k] + weights[k][j];
			            path[i][j]=path[i][k];
			          }
				}
			}
		}
	}
	public static ArrayList<Edge> findpath(int source,int destination,int[][] path,ArrayList<Integer> steinerWay,ArrayList<Edge> edgesTotal,double [][] graphG,ArrayList<Point>points) {
		//extract the paths(the successor nodes) between two nodes(hitpoints) with using path matrix that we created in Floyd-Warshall algo
		steinerWay.add(source);
		while(source!=destination) {
			//we continue browse in path until we see wither destination or source
			if(path[source][destination] == destination ||  path[source][destination] == source)
			{
				steinerWay.add(destination); // at the end of search , add destination and at same time create edge and add it to edgesTotal
				Edge f= new Edge(source, destination, graphG[source][destination]); 
				edgesTotal.add(f);
				return edgesTotal;
			}else {//if not add the middle point to steinerWay and create edge and continue.
				steinerWay.add(path[source][destination]);
				//create all source,path
				Edge e = new Edge(source, path[source][destination], graphG[source][destination]);
				edgesTotal.add(e);
				if(graphG[source][destination]==Double.POSITIVE_INFINITY) {
					System.out.println("steiner dosent have solution for this graph");
					System.exit(22);
				}
				source=path[source][destination];
			}		
		}
		return edgesTotal;
	}
	public static void UpdateRoots(RootFinder[] rf, int pointa, int pointb) 
    { 
	 //check the rank and put the root with bigger rank to root of other 
		//rank show the times we see that root 
        int rootpointa = findRoot(rf, pointa); 
        int rootpointb = findRoot(rf, pointb); 
     
        if (rf[rootpointa].rank < rf[rootpointb].rank) 
            rf[rootpointa].parent = rootpointb; 
        else if (rf[rootpointa].rank > rf[rootpointb].rank) 
            rf[rootpointb].parent = rootpointa; 
        // If the ranks are equal, then make one as root and increment 
        else
        { 
            rf[rootpointb].parent = rootpointa; 
            rf[rootpointa].rank++; 
        } 
    }
   //this is a recursive function which find the root/parent of each points 
	public static int findRoot(RootFinder[] rf, int hpt ) 
    { 
	 if (rf[hpt].parent != hpt) {// if there 
		 rf[hpt].parent = findRoot(rf,rf[hpt].parent); 
	 }    
       return rf[hpt].parent; 
    } 
	
    public static void createSteinerEdges(ArrayList<Edge> finalEdges, ArrayList<Integer> steinerWay,double [][] graphG) {
		int k=1;
		int i=0;
		while(steinerWay.size()>k) {
			int src=steinerWay.get(i); 
			int des =steinerWay.get(k); 
			Edge edg= new Edge(src,des,graphG[src][des]); 
			finalEdges.add(edg);
			i=k;
			k++;
		}	
	}
	//for convert edge to tree
	public static Tree2D ConvertTree(ArrayList<Edge> edgesTotal,ArrayList<Point> points) {
		int s=edgesTotal.get(0).getSourceIndex(); //get the source point of first Edge of edgesTotal
		return ConvertSubTree(points.get(s),edgesTotal,points);
	}	
    //recursive function for create a subtrees to show in graph
	public static Tree2D ConvertSubTree(Point startp,ArrayList<Edge> finalEdges,ArrayList<Point> points) {
		ArrayList<Point> MiddlePoints = new ArrayList<>();
		ArrayList<Edge> EdgesNew =  (ArrayList<Edge>) finalEdges.clone(); //put a copy of arraylist finalEdges to EdgesNew
		for (Edge eg : finalEdges) { //for each edge check and try to find commun point with startpoint , and when he find it , add it to middlePoints  
			Point source= points.get(eg.getSourceIndex());
			Point destination=points.get(eg.getDestIndex());
			if (source.equals(startp)) {
				MiddlePoints.add(destination);
				EdgesNew.remove(eg);
			} else {
				if (destination.equals(startp)) {
					MiddlePoints.add(source);
					EdgesNew.remove(eg);
				}
			}
		}
		//each middlepoints add as subtrees and continue to find sous-subtrees with new startpoint who is a middlePoints
		ArrayList<Tree2D> subtrees = new ArrayList<>();
		for (Point q : MiddlePoints) {
			subtrees.add(ConvertSubTree(q, EdgesNew,points));
		}
		return new Tree2D(startp, subtrees);
	}
	
	public static Edge findBestEdge(HashMap<Integer, Boolean> visitedPoint, double minden,ArrayList<Edge> hitsEdges,ArrayList<Point> points) {
		 Edge eg=null;
		 //find an edge with minimum cost and connect to our last edge that we considered
		 for(Edge hitedg:hitsEdges) {  //for each edges in hitedges		
        	 if(visitedPoint.containsKey(hitedg.getSourceIndex()) ^ visitedPoint.containsKey(hitedg.getDestIndex())) {  //if one of the source or destination point doesn't 'have already visited
	          double yy = (double)0;
	    	  //Point source=points.get(hitedg.getSourceIndex()); 
	    	  //Point dest =points.get(hitedg.getDestIndex());
	    	   if(visitedPoint.containsKey(hitedg.getSourceIndex())){ //if source of edge is visited
	    		   yy=hitedg.edgeCost();
	    	   }
	    	   if(visitedPoint.containsKey(hitedg.getDestIndex())) { //if destination edge is visited
	    		   yy=hitedg.edgeCost();
	    	   }
	    	   if(yy<minden) {
	    		   eg=hitedg;
	    		   minden=yy;
	    	   } 
        }
        	 
       }//end for
		 return eg;
	}
}