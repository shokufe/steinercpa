package algorithms;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;


public class DefaultTeam {
  public Tree2D calculSteiner(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {
	 //++++++++++++++++++++initialisation
	  int a=points.size();
	  double[][] graphG=new double [a][a]; //matrix of weights of points
	  int[][] path=new int[a][a]; //matrix of way 
	 Tools.CreateP(points,path,edgeThreshold,graphG); //initialise path matrix( each row has the same id as index of row )
	
	//++++++++++++++++++++end of initialisation++++++++++//
	 
	//+++ find the index of each hitpoints in points arrayList and put that in the new array named indexHit++++// 
		int[] indexhit =new int[hitPoints.size()]; 
		for (int i = 0; i < hitPoints.size();i++) {
			Point hit= hitPoints.get(i);
		    for(int j=0; j<points.size();j++) {
		    	Point orig =points.get(j);
		    	if(hit.getX()==orig.getX() &&hit.getY()==orig.getY()) { //if hitpoint(x,y)==points(x,y)
					indexhit[i]=j;
					}
				}
			}
	 //++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	/* this method with algorithme floyd-Warshall find the min paths for each deux nodes and update matrix graphG and path */	
	  Tools.shorthestPath(graphG, path,points);
	  long weightT=0;  //weight total  
	//++++++++++++++++++++++++++++++kruskal+++++++++++++++++++++++//
	//1- find all possible edges between hitpoints and put it in hitsEdges	
	  ArrayList<Edge> hitsEdges=new ArrayList<>();
	 for(int i=0;i<indexhit.length;i++) {
		for(int j=i+1;j<indexhit.length;j++) {
			int src=indexhit[i];
			int des=indexhit[j];
			Edge edg=new Edge(src,des,graphG[src][des]);
			hitsEdges.add(edg);
			}
	}
	
	//2- sort edgeshitpoints based on their weights
	hitsEdges.sort(Comparator.comparingDouble(Edge::edgeCost));
	 //2-1 RootFinder[] is for save the root of each node/branch of tree. we use this for finding circle		
	RootFinder rf[] = new RootFinder[points.size()]; 
	//initialisation RootFinder
	for(int i=0; i<points.size(); ++i) 
		 rf[i]=new RootFinder(); 
		for (int j = 0; j < points.size(); ++j) 
		   { 
			rf[j].parent = j; 
			rf[j].rank = 0; 
		  } 
	     int fk=0;
	     //time to choose the edges!  we choose edges from the hitedges based on their cost and if that make circle we denie that edge
	     ArrayList<Edge> steinerEdges =new ArrayList<Edge>();
		  steinerEdges.add(hitsEdges.get(0)); // add the first edge from hitedges
		  //continue until all hitedges visited
			 while(steinerEdges.size()<indexhit.length) { 
				  Edge edgLocal = hitsEdges.get(fk++);
				  int src =edgLocal.getSourceIndex(); //get the source id of edgeLocal
				  int des =edgLocal.getDestIndex(); ////get the destination id of edgeLocal
				  int x = Tools.findRoot(rf,src); //find the parent or root of point source
		          int y = Tools.findRoot(rf,des); //find the parent or root of point destination
		        //if they have the same parent means there is a circle and we denie it , if not we add it to steiner edge and update the parents/root
		          if(x!=y) { 
					  steinerEdges.add(edgLocal);
					  Tools.UpdateRoots(rf, x, y); //put the same parent for two new edge source and edge destination
					  weightT += edgLocal.edgeCost();  
				  }
			  }
		if(weightT >100000) //if we arrive at infinity path , means there is no way to visite a node of hits ,which means the steiner has no solution for that graph
		{
			System.out.println("steiner dosen't have answer in this graph");
		}
		else {
			System.out.println("weight total:"+weightT); 
		}
			  ArrayList<Integer> steinerWay=new ArrayList<Integer>();
			  ArrayList<Edge> edgesTotal=new ArrayList<Edge>();
			 //find the path between two hitpoints who is already sorted and have been put in steinerEdges
			  for(Edge af: steinerEdges) {
				  int source = af.getSourceIndex();
				  int destin = af.getDestIndex();
				  //this method get two points and retuns the path between them by help of path matrix that we made it in Floyd-Warshall algo
				  Tools.findpath(source, destin, path, steinerWay , edgesTotal, graphG,points);//put all edges should be visited in edgesTotal
			  }
			//  System.out.println("edgesTotal:"+edgesTotal.size());
	        //convert edgesTotal to tree
			  Tree2D steinerTree= Tools.ConvertTree(edgesTotal,points) ;
			  return steinerTree;
  }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+++++++++++++++++++++++BUDGET Restriction++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
 public Tree2D calculSteinerBudget(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {
	 //+++++++++++++++initialisation++++++++++++++++++++++++++//
	   int Budget =1664;
	  int a=points.size();
	  double[][] graphG=new double [a][a]; //matrix of weights of points
	  int[][] path=new int[a][a]; //matrix of way 
	 Tools.CreateP(points,path,edgeThreshold,graphG); //initialise path matrix( each row has the same id as index of row )
	//++++++++++++++++++++end of initialisation+++++++++++++++//
	//+++ find the index of each hitpoints in points arrayList and put that in the new array named indexHit++++// 
		int[] indexhit =new int[hitPoints.size()]; 
		for (int i = 0; i < hitPoints.size();i++) {
			Point hit= hitPoints.get(i);
		    for(int j=0; j<points.size();j++) {
		    	Point orig =points.get(j);
		    	if(hit.getX()==orig.getX() &&hit.getY()==orig.getY()) { //when hitpoint(x,y)==points(x,y)
					indexhit[i]=j;
					}
				}
			}
	 //++++++++++++++++++++++++++++++++++++++++++++++++++++++//
		/* this method with algorithme floyd-Warshall find the minimum paths for each two nodes and updates matrix graphG and path */	
	     Tools.shorthestPath(graphG, path,points);
		//++++++++++++++++++++++++++++++kruskal+++++++++++++++++++++++//
		 //1- find all possible edges between hitpoints and put it in hitsEdges
		  ArrayList<Edge> hitsEdges=new ArrayList<>();
		  for(int i=0;i<indexhit.length;i++) {
			  for(int j=i+1;j<indexhit.length;j++) {
				  int src=indexhit[i];
				  int des=indexhit[j];
				  Edge edg=new Edge(src,des,graphG[src][des]);
					 hitsEdges.add(edg);
				 }
		  	}
		  //2- sort edgeshitpoints based on their weights
		  hitsEdges.sort(Comparator.comparingDouble(Edge::edgeCost));
		  
		  double budgetLocal=0;
		  ArrayList<Edge> steinerEdges =new ArrayList<Edge>(); // choosen edges 
		 steinerEdges.add(hitsEdges.get(0));//add first edge from hitedge to steineredge
		 HashMap<Integer,Boolean> visitedPoint =new HashMap<>(); // add visited points 
	     // visit first points of first edge
		 visitedPoint.put(hitsEdges.get(0).getSourceIndex(), true); //put source of the first edge as visited
	     visitedPoint.put(hitsEdges.get(0).getDestIndex(), true); //put destination of the first edges as visited
	    //add first edge cost to budget
	     budgetLocal+=hitsEdges.get(0).edgeCost();
	    //by using algorithme kruskal and best next edges but also it should have the connection with older visited points each time we control the budget total
	     while(steinerEdges.size()<indexhit.length) { //this boucle continue until we see all hitedges visited or budget is finish
	    	 Edge edge=null;
	    	 double minden = Double.POSITIVE_INFINITY; //minimum density initialize by infinity
	    	 edge=Tools.findBestEdge(visitedPoint,minden,hitsEdges,points);//find the edge that connect to edges that we already visit 
	      //check budget
	    	if(edge.edgeCost()+budgetLocal>Budget) { //budget is finish so break the loop
	    	   break;}
	    	//else add edgeCost of new edge to budget
	       budgetLocal+=edge.edgeCost();
	       //Set src and destination points of chosen edge as visited
	       int src =edge.getSourceIndex(); 
	       visitedPoint.put(src, true);
		   int des =edge.getDestIndex(); 
		   visitedPoint.put(des, true);
	       steinerEdges.add(edge);
	       }//end of while
	     
	     //find the middle points from one source(hitpoint) to another with use algo Floyd-Warshall
	      ArrayList<Integer> steinerWay=new ArrayList<Integer>();
		  ArrayList<Edge> edgesTotal=new ArrayList<Edge>();
		 //like first part, find the middle edges
		  for(int i=0;i<steinerEdges.size();i++) { //for each edge find the shortest path in main graph
			  int source = steinerEdges.get(i).getSourceIndex();
			  int destin = steinerEdges.get(i).getDestIndex();
			  System.out.println(source+"-->"+destin);
			  Tools.findpath(source, destin, path, steinerWay , edgesTotal, graphG,points); 
		  }
		 System.out.println(">>>>all edges visited :"+edgesTotal.size());
		 Tree2D steinerTree= Tools.ConvertTree(edgesTotal,points) ;///convert to tree
		  return steinerTree;
	 	}				
	}
